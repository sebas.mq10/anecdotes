import React, { useState } from "react";
import ReactDOM from "react-dom/client";

const Button = ({ onClick, text }) => <button onClick={onClick}>{text}</button>;

const Title = ({ text }) => <h1>{text}</h1>;

const MostVotedAnecdote = ({ votes }) => {
  const mostVoted = Math.max(...votes); // desestructuro el array de votes y busco el mayor numero
  const highestVote = votes.indexOf(mostVoted); // retorna el primer indice en el que se puede encontrar el elemento dado, en este caso mostVoted es el maximo numero del array
  const mostVotedAnecdote = anecdotes[highestVote];

  if (mostVoted === 0) {
    return (
      <div>
        <p>No votes yet</p>
      </div>
    );
  } else {
    return (
      <div>
        <p>{mostVotedAnecdote}</p>
        <p>has {mostVoted} votes</p>
      </div>
    );
  }
};

const App = ({ anecdotes }) => {
  const [selected, setSelected] = useState(0);
  const [votes, setVotes] = useState(Array(anecdotes.length).fill(0));

  const getRandomAnecdote = () => {
    const random = Math.floor(Math.random() * anecdotes.length);
    setSelected(random);
  };

  const handleVote = () => {
    const newVotes = [...votes];
    newVotes[selected] += 1;
    setVotes(newVotes);
  };

  return (
    <div>
      <Title text="Anecdote of the day" />
      {anecdotes[selected]}
      <p>has {votes[selected]} votes</p>
      <Button onClick={handleVote} text="vote" />
      <Button onClick={getRandomAnecdote} text="next anecdote" />
      <Title text="Anecdote with most votes" />
      <MostVotedAnecdote votes={votes} />
    </div>
  );
};

const anecdotes = [
  "If it hurts, do it more often",
  "Adding manpower to a late software project makes it later!",
  "The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.",
  "Any fool can write code that a computer can understand. Good programmers write code that humans can understand.",
  "Premature optimization is the root of all evil.",
  "Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.",
];

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App anecdotes={anecdotes} />
  </React.StrictMode>
);
